const path = require('path');
const merge = require('webpack-merge');
const common = require('./webpack.common');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');

const browserSyncPlugin = new BrowserSyncPlugin({
  host: process.env.IP || 'localhost',
  port: process.env.PORT || 3000,
  server: {
    baseDir: ['./dist'],
  },
});

module.exports = merge(common, {
  mode: 'development',
  output: {
    filename: '[name]-bundle.js',
  },
  plugins: [browserSyncPlugin],
});
