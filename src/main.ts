import { TestScene } from './scenes/TestScene';

const config: GameConfig = {
  type: Phaser.AUTO,
  parent: 'main',
  width: 800,
  height: 600,
  resolution: 1,
  backgroundColor: '#FFFFFF',
  physics: {
    default: 'arcade',
    arcade: {
      gravity: { y: 300 },
      debug: false,
    },
  },
  scene: [TestScene],
};

const game = new Phaser.Game(config);
