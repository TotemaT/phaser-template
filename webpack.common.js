const path = require('path');
var webpack = require('webpack');
const CleanWebpackPlugin = require('clean-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

var phaserModule = path.join(__dirname, '/node_modules/phaser/');
var phaser = path.join(phaserModule, 'src/phaser.js');

const htmlWebpackPlugin = new HtmlWebpackPlugin({
  template: './src/index.html',
  title: 'Phaser Template',
});

const cleanWebpackPlugin = new CleanWebpackPlugin(['dist/*'], {
  verbose: false,
  dry: false,
});

const copyWebpackPlugin = new CopyWebpackPlugin([
  { from: './assets', to: './assets' },
]);

module.exports = {
  entry: {
    app: [path.resolve(__dirname, 'src/main.ts')],
  },
  output: {
    pathinfo: true,
    path: path.resolve(__dirname, 'dist'),
    publicPath: './',
  },
  target: 'web',
  module: {
    rules: [
      {
        test: /\.ts$/,
        loaders: ['babel-loader', 'awesome-typescript-loader'],
        include: path.join(__dirname, 'src'),
      },
    ],
  },
  node: {
    fs: 'empty',
    net: 'empty',
    tls: 'empty',
  },
  resolve: {
    extensions: ['.ts', '.js'],
    alias: {
      phaser: phaser,
    },
  },
  plugins: [cleanWebpackPlugin, copyWebpackPlugin, htmlWebpackPlugin],
};
